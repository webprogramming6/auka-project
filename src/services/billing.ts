import type Billing from "@/stores/types/Billing";
import http from "./axios";

function getBillings() {
  return http.get("/billing");
}

function saveBilling(billing: Billing) {
  return http.post("/billing", billing);
}

function updateBilling(id: string, billing: Billing) {
  return http.patch(`/billing/${id}`, billing);
}

function deleteBilling(id: string) {
  return http.delete(`/billing/${id}`);
}

function getBillingById(id: string) {
  return http.get(`/billing/${id}`);
}

export default {
  saveBilling,
  getBillingById,
  getBillings,
  updateBilling,
  deleteBilling,
};
