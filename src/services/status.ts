import type Status from "@/stores/types/Status";
import http from "./axios";

function getStatus() {
  return http.get("/status");
}

function getStatusById(id: number) {
  return http.get(`/status/${id}`);
}
export default { getStatus, getStatusById };
