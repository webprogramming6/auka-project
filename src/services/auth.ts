import type User from "@/stores/types/User";
import http from "./axios";

function login(username: string, password: string) {
  return http.post("/auth/login", { username, password });
}

export default { login };
