import type Order from "@/stores/types/Order";
import http from "./axios";
import type OrderItem from "@/stores/types/OrderItem";

function getOrders() {
  return http.get("/orders");
}

function getOrderItems() {
  return http.get("/orders");
}

function getOrderByTableId(id: number) {
  return http.get(`/orders/table/${id}`);
}
function getOrderItemsByOrderItemsId(id: number) {
  return http.get(`/orders/orderItem/${id}`);
}
function getOrderByStatusId(id: number) {
  return http.get(`/orders/status/${id}`);
}

function getOrderItemById(id: number) {
  return http.get(`/orders/orderItem/${id}`);
}

function saveOrder(order: Order) {
  return http.post("/orders", order);
}

function updateOrder(id: string, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function updateStatusOrderItems(id: string, orderItem: OrderItem) {
  return http.patch(`/orders/orderItem/${id}`, orderItem);
}

function deleteOrder(id: string) {
  return http.delete(`/orders/${id}`);
}

function deleteOrderItem(id: string) {
  return http.delete(`/orders/orderItem/${id}`);
}

function getOrderById(id: string) {
  return http.get(`/orders/${id}`);
}

export default {
  getOrders,
  saveOrder,
  deleteOrder,
  updateOrder,
  getOrderById,
  getOrderItems,
  getOrderItemById,
  getOrderByTableId,
  getOrderByStatusId,
  updateStatusOrderItems,
  getOrderItemsByOrderItemsId,
  deleteOrderItem,
};
