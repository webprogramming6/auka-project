import type Product from "@/stores/types/Product";
import http from "./axios";

function getProducts() {
  return http.get("/products");
}

function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  // formData.append("price","" + product.price);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function updateProduct(id: number, product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("price", `${product.price}`);
  // console.log(product.files);
  if (product.files) {
    formData.append("file", product.files[0]);
  }
  // return http.patch("/products" + id, product);
  return http.patch(`/products/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteProduct(id: number) {
  console.log(id);
  return http.delete(`/products/${id}`);
}
export default { getProducts, saveProduct, updateProduct, deleteProduct };
