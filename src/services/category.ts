import type Category from "@/stores/types/Category";
import http from "./axios";

function getCategories() {
  return http.get("/categories");
}

function savecategory(category: Category) {
  return http.post("/categories", category);
}

function updatecategory(id: number, category: Category) {
  return http.patch(`/categories/${id}`, category);
}

function deletecategory(id: number) {
  return http.delete(`/categories/${id}`);
}
export default { getCategories, savecategory, updatecategory, deletecategory };
