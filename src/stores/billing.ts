import { defineStore } from "pinia";
import type Billing from "./types/Billing";
import billingService from "@/services/billing";

export const useBillingStore = defineStore("billing", () => {
  const createBilling = async (billing: Billing) => {
    try {
      const res = await billingService.saveBilling(billing);
      return res.data;
    } catch (e) {
      console.log(e);
    }
  };

  return {
    createBilling,
  };
});
