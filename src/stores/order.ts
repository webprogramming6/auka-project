import { ref } from "vue";
import type Order from "./types/Order";
import { defineStore } from "pinia";
import { useMessageStore } from "./message";
import orderService from "@/services/order";
import type OrderItem from "./types/OrderItem";
import { useLoadingStore } from "./loading";
export const useOrderStore = defineStore("orders", () => {
  const messageStore = useMessageStore();
  const editedOrder = ref<Order>({ tableId: -1, orderItems: [] });
  const orderList = ref<Order[]>([]);
  const orderListByTableId = ref<Order[]>([]);
  const orderByOrderId = ref<Order>();
  const orderItemListByOrderId = ref<OrderItem[]>([]);
  const orderItemListByTableId = ref<OrderItem[]>([]);
  const orderItemByTableId = ref<OrderItem[]>([]);
  const orderListByStatus1 = ref<OrderItem[]>([]);
  const orderListByStatus2 = ref<OrderItem[]>([]);
  const orderListByStatus3 = ref<OrderItem[]>([]);
  const orderListByStatus4 = ref<OrderItem[]>([]);
  const finshedList = ref<OrderItem[]>([]);
  const selectedOrderItem = ref<OrderItem[]>([]);
  const orderItemList = ref<OrderItem[]>([]);
  const orderId = ref("-1");
  const selectedOrder = ref({ id: -1, statusId: -1 });
  const loadingStore = useLoadingStore();
  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrders();
      orderList.value = res.data;
      for (const i in orderList.value) {
        for (const j in orderList.value[i].orderItems) {
          orderItemList.value.push(orderList.value[i].orderItems[j]);
        }
      }
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
    loadingStore.isLoading = false;
  }
  async function getOrderByTableId(tableId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderByTableId(tableId);
      orderListByTableId.value = res.data;
      console.log("orderByTableId : ", orderListByTableId.value);
      for (const i in orderListByTableId.value) {
        for (const j in orderListByTableId.value[i].orderItems) {
          orderItemListByTableId.value.push(
            orderListByTableId.value[i].orderItems[j]
          );
        }
      }

      orderId.value =
        orderListByTableId.value[orderListByTableId.value.length - 1].id!;

      loadingStore.isLoading = false;
      return orderListByTableId.value;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
      loadingStore.isLoading = false;
    }
  }
  async function getOrderById(orderId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderById(orderId.toString());
      console.log("res.data", res.data);
      orderByOrderId.value = res.data;
      if (!orderByOrderId.value) return;

      orderItemListByOrderId.value = [];

      console.log("orderByOrderId : ", orderByOrderId.value);

      for (const item of orderByOrderId.value!.orderItems) {
        console.log("item : ", item);
        orderItemListByOrderId.value.push(item);
      }
      loadingStore.isLoading = false;
      return orderListByTableId.value;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
    loadingStore.isLoading = false;
  }
  async function changeTable(
    orderId: string,
    tableId: string,
    foodInCartList: OrderItem[]
  ) {
    try {
      const res = await orderService.getOrderById(orderId);
      const table = ref();
      table.value = res.data;
      const order: Order = {
        id: orderId,
        orderItems: foodInCartList,
        tableId: parseInt(tableId),
      };
      orderService.updateOrder(orderId, order);
    } catch (e) {
      console.log(e);
    }
  }
  async function closeOrder(id: string) {
    try {
      await orderService.deleteOrder(id);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }

  async function removeOrderIem() {
    try {
      console.log("srf", orderListByTableId.value);
      for (const item of orderByOrderId.value!.orderItems) {
        console.log("item id : ", item.id);
        await orderService.deleteOrderItem(JSON.stringify(item.id));
      }
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }

  async function getOrderByStatusId1(statusId: number) {
    try {
      const res = await orderService.getOrderByStatusId(statusId);
      orderListByStatus1.value = res.data;
      console.log("orderListByStatus1 : ", orderListByStatus1.value);
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }
  async function getOrderByStatusId2(statusId: number) {
    try {
      const res = await orderService.getOrderByStatusId(statusId);
      orderListByStatus2.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }
  async function getOrderByStatusId3(statusId: number) {
    try {
      const res = await orderService.getOrderByStatusId(statusId);
      orderListByStatus3.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }
  async function getOrderByStatusId4(statusId: number) {
    try {
      const res = await orderService.getOrderByStatusId(statusId);
      orderListByStatus4.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }
  const deleteOrder = (id: string): void => {
    const index = orderList.value.findIndex((item) => item.id === id);
    orderList.value.splice(index, 1);
  };

  async function saveOrder() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.saveOrder(editedOrder.value);
      orderId.value = res.data;
      console.log("Created Order!");
      return res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    // await getTables();
    // dialog.value = false;
    loadingStore.isLoading = false;
  }

  async function getOrderItemById(orderItemId: number) {
    try {
      const res = await orderService.getOrderItemsByOrderItemsId(orderItemId);
      orderListByTableId.value = res.data;
      console.log("orderItem : ", res.data);
      return orderListByTableId.value;
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }

  async function shiftOrderQueue(selectedOrder: number, qty: number) {
    try {
      const res = await orderService.getOrderItemById(selectedOrder);
      const product = ref();
      product.value = res.data;
      console.log("pdId : ", product.value);
    } catch (e) {
      console.log(e);
    }
  }

  async function onMounted() {
    await getOrderByStatusId1(1);
    await getOrderByStatusId2(2);
    await getOrderByStatusId3(3);
    await getOrderByStatusId4(4);
    window.location.reload();
  }

  async function updateStatusOrderItems(
    orderItemId: number,
    productId: number,
    qty: number,
    stausId: number
  ) {
    try {
      // const res = await orderService.getOrderItemsByOrderItemsId(orderItemId);
      const orderItem: OrderItem = {
        id: orderItemId,
        productId: productId,
        qty: qty,
        statusId: stausId,
        price: 0,
      };
      console.log("ljslgjsrilfgj", orderItem);
      orderService.updateStatusOrderItems(
        JSON.stringify(orderItemId),
        orderItem
      );
    } catch (e) {
      console.log(e);
    }
  }

  return {
    updateStatusOrderItems,
    shiftOrderQueue,
    orderListByTableId,
    changeTable,
    orderList,
    editedOrder,
    finshedList,
    selectedOrderItem,
    selectedOrder,
    getOrders,
    orderItemList,
    getOrderById,
    orderItemByTableId,
    saveOrder,
    getOrderByTableId,
    orderByOrderId,
    orderId,
    deleteOrder,
    onMounted,
    orderItemListByTableId,
    orderItemListByOrderId,
    closeOrder,
    getOrderItemById,
    getOrderByStatusId1,
    getOrderByStatusId2,
    getOrderByStatusId3,
    getOrderByStatusId4,
    orderListByStatus1,
    orderListByStatus2,
    orderListByStatus3,
    orderListByStatus4,
    removeOrderIem,
  };
});
