import { ref } from "vue";
import { defineStore } from "pinia";
import type Status from "./types/Status";
import statusService from "@/services/status";

export const useStatusStore = defineStore("status", () => {
  const status = ref<Status[]>([]);
  const editedStatus = ref<Status>({ name: "" });
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();

  async function getAllStatus() {
    // loadingStore.isLoading = true;
    try {
      const res = await statusService.getStatus();
      status.value = res.data;
      console.log("status value : ", status.value);
      console.log(res);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    // loadingStore.isLoading = false;
  }
  async function getStatusByid(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await statusService.getStatusById(id);
      status.value = res.data;
      return status.value;
      // console.log("status value : ", status.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    // loadingStore.isLoading = false;
  }

  // function editCategory(status: Status) {
  //   editedStatus.value = JSON.parse(JSON.stringify(status));
  // }

  return {
    getAllStatus,
    getStatusByid,
  };
});
