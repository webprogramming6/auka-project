import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import auth from "@/services/auth";
import router from "@/router";

export const useLoginStore = defineStore("login", () => {
  const loadingStore = useLoadingStore();
  const MessageStore = useMessageStore();
  const userStore = useUserStore();
  const loginName = ref("");
  // const  = computed(() => {
  //   const user = localStorage.getItem("user");
  //   if (user) {
  //     return true;
  //   }
  //   return false;
  // });
  //----------------------------
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      console.log(res);
      router.push("/table");
    } catch (e) {
      MessageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;

    // if (userStore.login(userName, password)) {
    //   loginName.value = userName;
    //   localStorage.setItem("loken", userName);
    // } else {
    //   MessageStore.showMessage("Login or Password incorrect");
    // }
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
