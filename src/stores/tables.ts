import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Table from "./types/Tables";
import orderService from "@/services/table";
import { useLoadingStore } from "./loading";

export const useTableStore = defineStore("table", () => {
  const loadingStore = useLoadingStore();
  const tables = ref<Table[]>([]);
  const editedtables = ref<Table>({ status: "" });
  const dialog = ref(false);
  const dialogChange = ref(false);

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedtables.value = { status: "" };
    }
  });

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getTables();
      tables.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    loadingStore.isLoading = false;
    return tables;
  }

  async function changeTableStatus() {
    loadingStore.isLoading = true;
    try {
      if (editedtables.value.id) {
        const res = await orderService.updateTable(
          editedtables.value.id,
          editedtables.value
        );
      } else {
        const res = await orderService.saveTable(editedtables.value);
      }
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    await getTables();
    dialog.value = false;
    loadingStore.isLoading = false;
  }

  function editTable(table: Table) {
    dialog.value = true;
    editedtables.value = JSON.parse(JSON.stringify(table));
  }

  async function deleteTable(id: number) {
    try {
      const res = await orderService.deleteTable(id);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    await getTables();
  }

  function cardColor(status: string) {
    if (status === "ready") {
      return "green";
    } else if (status === "on-use") {
      return "red";
    } else if (status === "clean") {
      return "orange";
    }
  }

  return {
    getTables,
    changeTableStatus,
    cardColor,
    dialog,
    editedtables,
    tables,
    dialogChange,
  };

  // const dialog = ref(false);
  // const dialogChange = ref(false);
  // const selectTable = ref<Tables>({ id: -1, status: "" });
  // const tables = ref<Tables[]>([
  //   { id: 1, status: "ready" },
  //   { id: 2, status: "ready" },
  //   { id: 3, status: "ready" },
  //   { id: 4, status: "ready" },
  //   { id: 5, status: "ready" },
  //   { id: 6, status: "ready" },
  //   { id: 7, status: "ready" },
  //   { id: 8, status: "ready" },
  //   { id: 9, status: "ready" },
  //   { id: 10, status: "ready" },
  //   { id: 11, status: "ready" },
  //   { id: 12, status: "ready" },
  //   { id: 13, status: "ready" },
  //   { id: 14, status: "ready" },
  //   { id: 15, status: "ready" },
  //   { id: 16, status: "ready" },
  //   { id: 17, status: "ready" },
  //   { id: 18, status: "ready" },
  //   { id: 19, status: "ready" },
  //   { id: 20, status: "ready" },
  // ]);
  // const saveTable = () => {
  //   const index = tables.value.findIndex((item) => {
  //     return item.id === selectTable.value.id;
  //   });
  //   tables.value[index] = selectTable.value;
  //   dialog.value = false;
  //   clear();
  // };
  // const clear = () => {
  //   selectTable.value = { id: -1, status: "" };
  // };
  // return { tables, dialog, dialogChange, selectTable, saveTable };
});
