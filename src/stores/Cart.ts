import { reactive, ref } from "vue";
import { defineStore } from "pinia";
import type Order from "./types/Order";
import orderService from "@/services/order";
import type Product from "./types/Product";
import type OrderItem from "./types/OrderItem";
import { useLoadingStore } from "./loading";
import { useOrderStore } from "@/stores/order";

export const useCartStore = defineStore("cart", () => {
  let foodInCartList = reactive<OrderItem[]>([]);
  const emptyFoodList = reactive<OrderItem[]>([]);
  const loadingStore = useLoadingStore();
  const orderStore = useOrderStore();
  let totalPrice = 0;

  async function addCart(pd: Product[], orderId: string) {
    loadingStore.isLoading = true;
    for (const item of pd) {
      const orderItem: OrderItem = {
        qty: item.count!,
        productId: item.id!,
        statusId: 1,
      };
      foodInCartList.push(orderItem);
    }
    addOrderItems(orderId, foodInCartList);
    await orderStore.getOrderById(parseInt(orderStore.orderId!));
    loadingStore.isLoading = false;
  }

  function clearCart() {
    foodInCartList = [];
    totalPrice = 0;
  }

  async function addOrderItems(orderId: string, foodInCartList: OrderItem[]) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderById(orderId);
      const table = ref();
      table.value = res.data;
      const order: Order = {
        id: orderId,
        orderItems: foodInCartList,
        tableId: table.value.tableId,
      };
      orderService.updateOrder(orderId, order);
    } catch (e) {
      console.log(e);
    }
    clearCart();
    await orderStore.getOrderById(parseInt(orderStore.orderId!));
    loadingStore.isLoading = false;
  }

  return {
    emptyFoodList,
    foodInCartList,
    addCart,
    clearCart,
    addOrderItems,
    totalPrice,
  };
});
