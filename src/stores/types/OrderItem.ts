export default interface OrderItem {
  id?: number;
  name?: string;
  qty: number;
  price?: number;
  productId?: number;
  statusId: number;
}
