import type Order from "./Order";
import type User from "./User";

export default interface Billing {
  id?: number;
  total_price: number;
  cash: number;
  change: number;
  orderId?: string;
  role: string;
}
