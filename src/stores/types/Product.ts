import type Status from "./Status";

export default interface Product {
  id?: number;
  name: string;
  price?: number;
  image?: string;
  count?: number;
}
