import type orderItem from "./OrderItem";

export default interface order {
  id?: string;
  tableId: number;
  amount?: number;
  total?: number;
  orderItems: orderItem[];
}
