import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/stores/types/User";
import userService from "@/services/user";
import axios from "axios";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const useUserStore = defineStore("user", () => {
  const users = ref<User[]>([]);
  const editedUsers = ref<User>({ login: "", name: "", password: "" });
  const dialog = ref(false);
  const isTable = ref(true);
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedUsers.value = { login: "", name: "", password: "" };
    }
  });

  async function getUsers() {
    // loadingStore.isLoading = true;
    try {
      // const res = await axios.get("http://localhost:3000/users");
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(users.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function saveUsers() {
    try {
      if (editedUsers.value.id) {
        const res = await userService.updateUser(
          editedUsers.value.id,
          editedUsers.value
        );
      } else {
        const res = await userService.saveUser(editedUsers.value);
      }
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    await getUsers();
    dialog.value = false;
  }

  function editUsers(user: User) {
    dialog.value = true;
    editedUsers.value = JSON.parse(JSON.stringify(user));
  }

  async function deleteUsers(id: number) {
    try {
      const res = await userService.deleteUser(id);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    await getUsers();
  }
  return {
    users,
    getUsers,
    dialog,
    editedUsers,
    saveUsers,
    editUsers,
    deleteUsers,
    isTable,
  };
});

// export const useUserStore = defineStore("user", () => {
//   const dialog = ref(false);
//   const isTable = ref(true);
//   const edited = ref<User>({ id: -1, login: "", name: "", password: "" });
//   let lastId = 4;
//   const users = ref<User[]>([
//     { id: 1, login: "admin", name: "Admin", password: "Pass@1234" },
//     { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
//     { id: 3, login: "user2", name: "User 2", password: "Pass@1234" },
//   ]);
//   const deleteUser = (id: number): void => {
//     const index = users.value.findIndex((item) => {
//       return item.id === id;
//     });
//     users.value.splice(index, 1);
//   };

//   const login = (loginName: string, password: string): boolean => {
//     const index = users.value.findIndex((item) => item.login == loginName);
//     if (index >= 0) {
//       const user = users.value[index];
//       if (user.password == password) {
//         return true;
//       }
//       return false;
//     }
//     return false;
//   };

//   const saveUser = () => {
//     if (edited.value.id < 0) {
//       edited.value.id = lastId++;
//       users.value.push(edited.value);
//     } else {
//       const index = users.value.findIndex((item) => {
//         return item.id === edited.value.id;
//       });
//       users.value[index] = edited.value;
//     }
//     dialog.value = false;
//     clear();
//   };

//   const editUser = (user: User) => {
//     edited.value = { ...user };
//     dialog.value = true;
//   };

//   const clear = () => {
//     edited.value = { id: -1, login: "", name: "", password: "" };
//   };
//   return {
//     users,
//     deleteUser,
//     dialog,
//     edited,
//     clear,
//     saveUser,
//     editUser,
//     isTable,
//     login,
//   };
// });
