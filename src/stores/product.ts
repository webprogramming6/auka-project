import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "./types/Product";
import productService from "@/services/product";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isProduct = ref(true);
  const productList = ref<Product[]>([]);
  const editedProduct = ref<Product & { files: File[] }>({
    name: "",
    categoryId: 1,
    price: 0,
    image: "burger.png",
    files: [],
  });
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = {
        name: "",
        categoryId: 1,
        price: 0,
        image: "burger.png",
        files: [],
      };
    }
  });
  const selectProduct = ref<Product>({
    id: -1,
    name: "",
    categoryId: 1,
    price: 0,
    count: 0,
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      productList.value = res.data;
      console.log(productList.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล  User ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  async function deleteProduct(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Product ได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    dialog,
    selectProduct,
    isProduct,
    editedProduct,
    getProducts,
    productList,
    saveProduct,
    deleteProduct,
    editProduct,
  };
});
